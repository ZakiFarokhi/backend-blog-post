/** @format */

const { user: User } = require('../models');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const moment = require('moment');
const MailChecker = require('mailchecker');

exports.signup = async (req, res, next) => {
  const { name, email, username, password } = req.body;
  // const username = name.trim().replace(/\s+/g, '-').toLowerCase();

  if (!name || !email || !password || !username) {
    return res.status(401).json({
      message: 'Please provide name, email and password.',
    });
  } else {
    const hashedPassword = await bcrypt.hash(password, 12);
    if (!MailChecker.isValid(email)) {
      return res.status(401).json({
        message: 'Please provide a valid email.',
      });
    } else {
      await User.findOne({ where: { email } }).then((user) => {
        if (user) {
          return res.status(401).json({
            message: 'Email already exists.',
          });
        } else {
          User.create({
            name,
            email,
            username,
            password: hashedPassword,
          })
            .then((user) =>
              res.status(201).json({
                message: 'User created successfully.',
                userId: user.id,
              })
            )
            .catch((err) =>
              res.status(500).json({
                message: 'Failed to create account.',
                error: err.message,
              })
            );
        }
      });
    }
  }
};

exports.login = async (req, res, next) => {
  const { username, password } = req.body;
  if (!username || !password) {
    return res.status(401).json({
      message: 'Please provide username and password.',
    });
  } else {
    const user = await User.findOne({ where: { username } });

    // console.log('ini hasil user', user);

    if (!user) {
      return res.status(401).json({
        message: 'User not found.',
      });
    }

    const isPasswordValid = await bcrypt.compare(password, user.password);

    if (!isPasswordValid) {
      return res.status(401).json({
        message: 'Invalid password.',
      });
    }

    const token = jwt.sign(
      {
        userId: user.id,
        userUsername: user.username,
      },
      'supersecret',
      {
        expiresIn: '1h',
      }
    );

    return res.status(200).json({
      message: 'Login successful.',
      data: {
        token,
        userId: user.id,
        expired_at: moment().add(1, 'h').format('YYYY-MM-DD HH:mm:ss'),
      },
    });
  }
};
