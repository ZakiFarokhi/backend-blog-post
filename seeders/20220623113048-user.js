/** @format */

'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert(
      'users',
      [
        {
          name: 'John Doe',
          username: 'johndoe2',
          email: 'johndoe@gmail.com',
          password: 'qwerty',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Alex Edwards',
          username: 'alexeds3',
          email: 'alexedwards@gmail.com',
          password: 'thisisedward24',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
