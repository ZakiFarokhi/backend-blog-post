/** @format */

const express = require('express');
const app = express();
const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('docs.yml');
const bodyParser = require('body-parser');
const authRouter = require('./routes/auth');

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(authRouter);

app.all('*', (req, res) => {
  res.redirect('/docs');
});

app.listen(process.env.PORT || 3000, () => {
  console.log('Server is running on port', process.env.PORT || 3000);
});
