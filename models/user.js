/** @format */

'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.post, {
        foreignKey: 'author_id',
        as: 'author_post',
      });
      this.hasMany(models.follow, {
        foreignKey: 'follower_id',
        as: 'user_follow',
      });
      // this.hasMany(models.block_list, {
      //   foreignKey: 'user_id',
      //   as: 'block_user',
      // });
    }
  }
  user.init(
    {
      name: DataTypes.STRING,
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'user',
    }
  );
  return user;
};
