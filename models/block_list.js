/** @format */

'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class block_list extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // this.belongsTo(models.user, {
      //   foreignKey: 'user_id',
      //   as: 'user_follow',
      // });
    }
  }
  block_list.init(
    {
      user_id: DataTypes.INTEGER,
      block_user_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: 'block_list',
    }
  );
  return block_list;
};
